module go.fuchsia.dev/jiri

go 1.20

require (
	github.com/google/go-cmp v0.5.8
	golang.org/x/net v0.0.0-20190909003024-a7b16738d86b
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
)

require golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
